# DEPRECATED

This repository is deprecated in favor of [https://bitbucket.org/payyoag/payyo-php-sdk](https://bitbucket.org/payyoag/payyo-php-sdk) as well as 
`payyo/php-sdk` is supposed to be used instead of `trekkpay/php-sdk`

# TrekkPay PHP SDK

The TrekkPay PHP SDK including an API client library

## Installation

    composer require trekkpay/php-sdk

## Configuration

### Simple client

    <?php
    use TrekkPay\Sdk\ApiClient\Client;
    use TrekkPay\Sdk\ApiClient\Credentials;
    
    $credentials = new Credentials(
        getenv('API_PUBLIC_KEY'),
        getenv('API_SECRET_KEY')
    );
    
    $apiClient = new Client($credentials);

### Client with Redis cache and Monolog logging

    <?php
    use Doctrine\Common\Cache\RedisCache;
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    use TrekkPay\Sdk\ApiClient\Client;
    use TrekkPay\Sdk\ApiClient\Credentials;
    
    $redis = new \Redis();
    $redis->pconnect('127.0.0.1', 6379);
    
    $cache = new RedisCache();
    $cache->setRedis($redis);
    
    $credentials = new Credentials(
        getenv('API_PUBLIC_KEY'),
        getenv('API_SECRET_KEY')
    );

    $logger = new Logger('api-client');
    $logger->pushHandler(new StreamHandler('api-client.log'));
    
    $apiClient = Client::newCachingClient($credentials, $cache, $logger);

## Usage

### Creating a transaction

We recommend doing 2-step payment by first authorizing and then capturing a transaction. This rules out accidental
double charges.

    $response = $apiClient->transaction()->pay([
        'merchant_id'        => 1,      // use your merchant ID
        'merchant_reference' => 'test_order',
        'description'        => 'City Sightseeing',
        'currency'           => 'USD',
        'amount'             => 3000,   // => $30
        'funding_instrument' => [
            'type'    => 'credit_card',
            'number'  => '1111 2222 3333 4444',
            'expires' => '2020-04',
            'cvc'     => '123',
            'holder'  => 'John Doe',
        ]
    ]);
    
    $transactionId = $response->getValue('result.transaction_id');

    $apiClient->transaction()->capture($transactionId);

### Retrieving transaction details

    $response = $apiClient->transaction()->getDetails($transactionId);
    $transaction = $response->getValue('result');
    
    var_dump(
        $transaction['status'],
        $response->getValue('result.status')
    );


### Caching requests

    $apiClientThatCachesForOneMinute = $apiClient->withCache(60);
    $response = $apiClientThatCachesForOneMinute->transaction()->getDetails($transactionId);

### Built-in RPC methods

The following methods are currently supported:

    $this->client->transaction()->getDccQuote(array $params);
    $this->client->transaction()->initialize(array $params);
    $this->client->transaction()->authorize(array $params);
    $this->client->transaction()->void(string $transactionId);
    $this->client->transaction()->capture(string $transactionId, array $params);
    $this->client->transaction()->pay(array $params);
    $this->client->transaction()->reverse(string $transactionId, array $params);
    $this->client->transaction()->abort(string $transactionId);
    $this->client->transaction()->getDetails(string $transactionId, array $params);
    $this->client->transactions()->search(int|int[] $merchantIds, array $params);
    $this->client->transactions()->getStats(int|int[] $merchantIds, array $params);

But you can also call other RPC methods using the `request()` method.

    $this->client->request(string $method, array $params);

## Contributions

All contributions are welcomed through pull requests.

Please run tests (`vendor/bin/phpunit`) and Coding Style fixer (`vendor/bin/php-cs-fixer fix src --rules=@Symfony`)
before submitting.

## License

MIT. See LICENSE file.
