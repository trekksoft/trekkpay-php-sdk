<?php

namespace TrekkPay\Sdk\ApiClient\Exceptions;

class RuntimeException extends Exception
{
}
