<?php

namespace TrekkPay\Sdk\ApiClient\Http;

use TrekkPay\Sdk\ApiClient\Exceptions\RuntimeException;

class ConnectionError extends RuntimeException
{
}
