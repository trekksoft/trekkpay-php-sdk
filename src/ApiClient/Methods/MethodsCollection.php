<?php

namespace TrekkPay\Sdk\ApiClient\Methods;

use TrekkPay\Sdk\ApiClient\Client;
use TrekkPay\Sdk\ApiClient\Http\Response;

abstract class MethodsCollection
{
    /** @var Client */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method
     * @param array  $params
     *
     * @return Response
     */
    protected function request($method, array $params)
    {
        return $this->client->request($method, $params);
    }
}
