<?php

namespace TrekkPay\Sdk\Tests\ApiClient;

use PHPUnit\Framework\TestCase;
use TrekkPay\Sdk\ApiClient\Credentials;
use TrekkPay\Sdk\ApiClient\Http\Request;

final class RequestTest extends TestCase
{
    /** @test */
    public function request_signature_is_calculated_correctly()
    {
        $data = \GuzzleHttp\json_encode([
            'jsonrpc' => '2.0',
            'method' => 'transaction.getDetails',
            'params' => [
                'transaction_id' => 'tra_2f4bbf77c39017ba6b7bc9298672',
            ],
            'id' => 1,
        ]);

        $request = new Request('POST', '/v1');
        $request->getBody()->write($data);
        $request->getBody()->rewind();
        
        $request = $request->withAuthorizationHeader(new Credentials(
            'api_3f77c29ba6b7b86729017bbc92f4',
            'sec_22f4b9867017ba6b7bf77c39bc92'
        ));
        
        $this->assertSame(
            'Basic YXBpXzNmNzdjMjliYTZiN2I4NjcyOTAxN2JiYzkyZjQ6NTVlZDNhM2ZjZWJiOGE1ZDA4OTU1NTgwNGNhNTc1NjVlNGRjOTQ5YjQ0MDZlZjczNTU0NjE2MWE5ZmE1NzRiZA==',
            $request->getHeaderLine('Authorization')
        );
    }
}
