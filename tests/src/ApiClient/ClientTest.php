<?php

namespace TrekkPay\Sdk\Tests\ApiClient;

use Doctrine\Common\Cache\ArrayCache;
use PHPUnit\Framework\TestCase;
use TrekkPay\Sdk\ApiClient\Client;
use TrekkPay\Sdk\ApiClient\Credentials;
use TrekkPay\Sdk\ApiClient\Http\Client as HttpClient;
use Psr\Http\Message\RequestInterface as Request;
use TrekkPay\Sdk\ApiClient\Http\Response;

final class ClientTest extends TestCase
{
    /** @var HttpClient */
    private $httpClient;

    /** @var Client */
    private $client;

    /** @test */
    public function i_can_create_a_caching_client()
    {
        $cache = new ArrayCache();
        $client = Client::newCachingClient(new Credentials('', ''), $cache, null, $this->httpClient);

        // write
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame(0, $cache->getStats()['misses']);
        $this->assertSame(0, $cache->getStats()['hits']);
        // read
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame(0, $cache->getStats()['misses']);
        $this->assertSame(0, $cache->getStats()['hits']);

        $client = $client->withCache(60);

        // write
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame(2, $cache->getStats()['misses']); // it's 2 because one is a fetch from \Doctrine\Common\Cache\CacheProvider::getNamespaceVersion
        $this->assertSame(0, $cache->getStats()['hits']);
        // read
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame(1, $cache->getStats()['hits']);
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame(2, $cache->getStats()['hits']);
        $client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame(3, $cache->getStats()['hits']);
    }

    /** @test */
    public function i_can_make_successful_requests()
    {
        $response = $this->client->transaction()->getDetails('tra_2f4bbf77c39017ba6b7bc9298672');

        $this->assertSame($response->getValue('jsonrpc'), '2.0');
        $this->assertSame($response->getValue('result.transaction_id'), 'tra_2f4bbf77c39017ba6b7bc9298672');
        $this->assertSame($response->getValue('result.status'), 'authorized');
        $this->assertSame($response->getValue('id'), 1);
    }

    /**
     * @test
     * @expectedException \TrekkPay\Sdk\ApiClient\RequestError
     * @expectedExceptionMessage Merchant not allowed
     */
    public function i_handle_user_errors_properly()
    {
        $this->client->transactions()->search(1);
    }

    /**
     * @test
     * @expectedException \TrekkPay\Sdk\ApiClient\Http\ConnectionError
     * @expectedExceptionMessage JSON-RPC request failed. Got status code 500
     */
    public function i_handle_connection_errors_properly()
    {
        $this->client->transactions()->getStats(1);
    }

    public function setUp()
    {
        $this->httpClient = new class() implements HttpClient {
            public function request(Request $request)
            {
                $body = $request->getBody();
                $body->rewind();
                $data = \GuzzleHttp\json_decode($body->getContents());

                switch ($data->method) {
                    case 'transaction.getDetails':
                        return new Response(200, ['contant-type' => 'application/json'], \GuzzleHttp\json_encode([
                            'jsonrpc' => '2.0',
                            'result' => [
                                'transaction_id' => 'tra_2f4bbf77c39017ba6b7bc9298672',
                                'status' => 'authorized',
                            ],
                            'id' => $data->id,
                        ]));

                    case 'transactions.search':
                        return new Response(200, ['contant-type' => 'application/json'], \GuzzleHttp\json_encode([
                            'jsonrpc' => '2.0',
                            'error' => [
                                'code' => -32602,
                                'message' => 'Merchant not allowed',
                            ],
                            'id' => $data->id,
                        ]));

                    case 'transactions.getStats':
                        return new Response(500, [], 'Internal servier error');

                    default:
                        throw new \LogicException('Not implemented');
                }
            }
        };

        $this->client = new Client(
            new Credentials('', ''),
            null,
            $this->httpClient
        );
    }
}
